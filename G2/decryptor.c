#include "csapp.c"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* usage: ./echoclient host port */


int getNthPrime (int N)
{
	int primes[999] = { 2, 3 };
    int n;
    int x;
    int i;

    for (n = 2; n < N+1; n++) {
        for (x = primes[n - 1] + 2; ; x += 2) {
            int prime = 1;
            for (i = 0; i < n; i++) {
                int p = primes[i];
                if (p * p > x) {
                    break;
                }
                if (x % p == 0) {
                    prime = 0;
                    break;
                }
            }
            if (prime==1) {
                primes[n] = x;
                break;
            }
        }
    }

    return (primes[N]);
}

int mod_inverse(int a, int b)
{
    int b0 = b, t, q;
    int x0 = 0, x1 = 1;
    if (b == 1) return 1;
    while (a > 1) {
        q = a / b;
        t = b, b = a % b, a = t;
        t = x0, x0 = x1 - q * x0, x1 = t;
    }
    if (x1 < 0) x1 += b0;
    return x1;
}

int gcd(int a, int b){ //If the result of this is 1, numbers are coprime
    int i;
    int gcd;
    for (i=1; 1<=a && i<=b; i++){
        if (a%i==0 && b%i==0)
            gcd = i;
    }
    return gcd;
}

// vvv New Stuff vvv

//Finds a random, smaller number that is coprime with n
int coprime(int n)
{
    
    int randomInt;
    do {
        randomInt = rand() % n;
    } while (gcd(randomInt, n) != 1);
    return randomInt;
}

//calculates (a^b)%c
int modulo(int a, int b, int c)
{
    int temp=1;
    int i;

    for(i=1; i<b; i++){
        temp = temp*a;
        temp = temp%c;
    }

    return (temp);
}

//Finds the number of coprime numbers with n
int totient(int n)
{
    int i;
    int counter=1;
    for (i=2; i<n; i++){
        if (gcd(i, n) == 1){
            counter++;
        }
    }
    return counter;
}


int genKeyPt2(int phiNum)
{
    int pub_e=3;
    while (phiNum % pub_e == 0){
        pub_e+=2;
    }
    return pub_e;    
}


int main(int argc, char **argv)
{ 
    srand(time(NULL));
    int clientfd, port; 
    char *host, buf[MAXLINE], code[MAXLINE]; 
    int j,i,x,y,z,a,b,c,m,n,e,d,publicKey,privateKey;

    printf("Please enter the private key value:\n");
    scanf ("%d",&d);    
    printf("Please enter the corresponding c value:\n");
    scanf ("%d",&c);

    while (1)
    {
        printf("Enter a number to decode.\n");
        scanf("%d", &z);

        int answer = modulo(z,d,c);
        printf("%d\n", answer);
    }
    
    exit(0); 
} 




Bryce Eller
Chris Gierz
CS283 - G2 - Network Programming

So me and Chris had a lot of trouble with this lab. We have many of the components working seperately but
couldn't really get them to work together.

We have the chat program working correctly. It sends data to the server which recieves the data and
sends it back. In the client file we also have the RSA encryption algorithm that encodes the string,
but we couldn't figure out how to get the encoded characters to send to the server. We output the encoded
characters, but only send the raw strings.

We wrote the code for the decryption software, but it isn't working correctly. I thought we followed all
directions and the algorithm makes mathematical sense, but we just keep getting incorrect answers and 
can't figure out why.

The brute force cracker is pretty awesome. It takes in a public key and gets the private key very quickly.
I suspect it would actually break the encoding, if our decoding algorithm would give proper answers. It always
gets the private key though, which is good.

We also can't figure out how to set variable values with makefiles so we were unfortunately unable to meet
that requirement. You can set ip and ports via program arguments.

Also I submitted this same thing in folder G2 and A2. The website said two different things so I thought I'd
just play it safe.
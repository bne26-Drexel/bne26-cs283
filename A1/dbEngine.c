#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> 
#include <fcntl.h>

//Stores data with CSV files
//Each row is a column. Each field is seperated by a comma, with the first field being the column title

//Thank you Mr.HMJD on StackOverflow. I didn't write this function.
//Takes a string and a character delimeter. returns an array with the tokens around the delimeters
char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    count += last_comma < (a_str + strlen(a_str) - 1);
    count++;
    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        *(result + idx) = 0;
    }

    return result;
}

//Creates a new table with columns
//First argument is the name of the table including file extention
//Second argument is the column names
//Third argument is the number of columns
int create(char tablename[], const char *fields[], int size){
	open(tablename, O_RDWR|O_CREAT, 0777); 

	FILE *f = fopen(tablename, "a");
	int i;
	for (i=0; i<size; i++){
		fprintf(f, "%s\n", fields[i]);
	}
}

//Deletes a table
//First argument is the name of the table
int drop(char tablename[]){
	return remove(tablename);
}

//Updates a certain value in a table
//First argument is the table name
//Second argument is the value to add
//Third argument is the column to add it in
//Fourth argument is the value to remove
int update(char tablename[], char newValue[], char column[], char value[]){
	FILE *rm;
	FILE *new;
    char buf[2];
    int res;
	rm = fopen(tablename, "r");
	new = fopen("temp.txt", "a");
	char word[1000]="";
	int i;

	int valueMatch=0;

	if (rm != NULL) {
	    while (!feof(rm)) {
	        res = fread(buf, 1, (sizeof buf)-1, rm);
	        buf[res] = 0;
	        int wrote = 0;

	        if (buf[0] == ','){
	        	if(strcmp(word,column) == 0){
	        		valueMatch=1;
	        		fprintf(new,"%s,", word);
	        	}
	        	else if(strcmp(word,value)==0 && valueMatch==1){
					fprintf(new,"%s,", newValue);
	        	}
	        	else{
	        		fprintf(new,"%s,", word);
	        	}	

	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        }
	        else if (buf[0] == '\n'){
	        	if(strcmp(word,value)==0 && valueMatch==1){
					fprintf(new,"%s\n", newValue);
	        	}
	        	else{
	        		fprintf(new,"%s\n", word);
	        	}	

	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        }
	        strcat(word,buf);
	    }
	    fprintf(new, "%s", word);
	    fclose(rm);
	}

	remove(tablename);
	rename("temp.txt", tablename);

	return 1;
}

//Deletes a column from a table
//First argument is the name of the table
//Second argument is the name of the column to delete
int deletefrom(char tablename[], char column[]){
	FILE *rm;
	FILE *new;
    char buf[2];
    int res;
	rm = fopen(tablename, "r");
	new = fopen("temp.txt", "a");
	char word[1000]="";
	int i;

    int columnMatch = 0;

	if (rm != NULL) {
	    while (!feof(rm)) {
	        res = fread(buf, 1, (sizeof buf)-1, rm);
	        buf[res] = 0;

	        if (buf[0] == ',' && columnMatch==0){
	        	if(strcmp(word,column) == 0){
	        		columnMatch=1;
	        	}
				else{
					fprintf(new,"%s,",word);
				}

	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        	
	        }
	        else if (buf[0] == '\n'){

	        	if(columnMatch == 0){
	        		fprintf(new,"%s\n", word);
	        	}
	        	else{
	        		columnMatch=0;
	        	}

	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        }
	        strcat(word,buf);
	    }
	    fprintf(new, "%s", word);
	    fclose(rm);
	}

	remove(tablename);
	rename("temp.txt", tablename);

	return 1;
}

//Selects all values in a certain table column
//First argument is the name of the table
//Second argument is the name of the column to select
//Third argument is the array that will contain the contents of the selected column 
int selectfrom(char tablename[], char column[], char *results[]){
	FILE *rm;
	FILE *new;
    char buf[2];
    int res;
	rm = fopen(tablename, "r");
	char word[1000]="";
	int i;

    int columnMatch = 0;
    int valueCount = 0;

	if (rm != NULL) {
	    while (!feof(rm)) {
	        res = fread(buf, 1, (sizeof buf)-1, rm);
	        buf[res] = 0;

	        if (buf[0] == ','){
	        	if(columnMatch == 0 && strcmp(word,column) == 0){
	        		columnMatch=1;
	        	}
	        	else if (columnMatch == 1){
	        		results[valueCount] = word;
	        		valueCount=valueCount+1;
	        	}

	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        	
	        }
	        else if (buf[0] == '\n'){
	        	if (columnMatch == 1){
		        	fclose(rm);
		        	return valueCount;
	        	}
	        	else{
	        		memset(word, 0, sizeof(word));
	        		memset(buf, 0, sizeof(buf));
	        	}
	        }
	        strcat(word,buf);
	    }
	    fclose(rm);
	}

	return 0;
}

//Inserts data into a table
//First argument is the name of the table
//Second argument is an array of values to add.
//*** ARRAY MUST BE IN FORMAT: columnname=value
//Third argument is the size of the values array
int insert(char tablename[], const char *values[], int size){
	FILE *rm;
	FILE *new;
    char buf[2];
    int res;
	rm = fopen(tablename, "r");
	new = fopen("temp.txt", "a");
	char word[1000]="";
	int i;

	char** tokens;
	char* tokenlist[10];

	for(i=0; i<size; i++){
		char splitme[100];
		strcpy(splitme, values[i]);
		tokens = str_split(splitme, '=');
		tokenlist[i*2] = tokens[0];
		tokenlist[(i*2)+1] = tokens[1];
	}	

	if (rm != NULL) {
	    while (!feof(rm)) {
	        res = fread(buf, 1, (sizeof buf)-1, rm);
	        buf[res] = 0;
	        int wrote = 0;

	        if (buf[0] == ','){
		        for (i=0; i<size; i++){
		        	if(strcmp(word,tokenlist[i*2]) == 0){
		        		fprintf(new,"%s,%s,", word, tokenlist[(i*2)+1]);
		        		wrote=1;
		        	}
		        }
	        	if (wrote==0){
	        		fprintf(new,"%s,",word);
	        	}
	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        	
	        }
	        else if (buf[0] == '\n'){
	        	printf("%s", word);
	        	for (i=0; i<size; i++){
		        	if(strcmp(word,tokenlist[i*2]) == 0){
		        		fprintf(new,"%s,%s\n", word, tokenlist[(i*2)+1]);
		        		wrote=1;
		        	}
		        }
	        	if(wrote==0){
	        		fprintf(new,"%s\n",word);
	        	}
	        	memset(word, 0, sizeof(word));
	        	memset(buf, 0, sizeof(buf));
	        	
	        }
	        strcat(word,buf);
	    }
	    fprintf(new, "%s", word);
	    fclose(rm);
	}

	remove(tablename);
	rename("temp.txt", tablename);

	return 1;
}

int main(int argc, char *argv[]){

	if (argc == 3 && (strcmp(argv[1], "create")) == 0){
		printf("TEST");
		const char *columns[] = {"one","two","three"};
		create(argv[2], columns, 3);
	}

	//const char *columns[] = {"one","two","three", "test"};
	//create("testTable.txt", columns, 4); 
	//Creates table
	
	//const char *insertme[] = {"one=wordincolumnone", "three=data", "test=evenmoredata"};
	//insert("testTable.txt", insertme, 3);
	//Adds data to table

	//char *selectedData[999];
	//selectfrom("testTable.txt", "one", selectedData);
	//printf("%s,%s\n", selectedData[0], selectedData[1]);
	//Prints data from column 1

	//deletefrom("testTable.txt", "one");
	//Deletes all data in column 1

	//update("testTable.txt", "THISISBRANDNEWWOW", "three", "moredata");
	//Replaces the second peice of data in column 3

	//drop("testTable.txt");
	//Uncomment the above to delete the file*/
}


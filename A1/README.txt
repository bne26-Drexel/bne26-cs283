Bryce Eller
CS283
DbEngine

My code works mostly as expected for all the commands except the join.
Select, delete, update, insert, create, and drop all work perfectly.
You can also create a table via command line parameters "create" "tablename.txt".

The only thing I noticed is that If I try and create and edit a table in the same
program, it doesn't work. Couldn't figure out why, but it works fine if you run a
command, then change the main to edit the table, and then run it again.

I used a csv file format to store the data. Each row is a column. Each field is
seperated by a comma, with the first field being the column title. 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void swap(int* a, int* b) {
    int c = *a;
    *a = *b;
    *b = c;
}

int* sort(int* a, int size){
	int i;
	int j;

	for(i=0;i<size-1;i++){
		for(j=0; j<size-1;j++){
			if (a[j]>a[j+1]){
				swap(&a[j],&a[j+1]);
			}
		}
	}
	return a;
}

int main(){

	int* array = malloc(sizeof(int)*10);
	int i;

	array[0] = 5;
	array[1] = 4;
	array[2] = 6;
	array[3] = 3;
	array[4] = 7;
	array[5] = 2;
	array[6] = 8;
	array[7] = 1;
	array[8] = 9;
	array[9] = 0;
	
	
	for (i=0; i<10; i++){
		printf("%d\n", array[i]);
	}

	array=sort(array,10);
	
	for (i=0; i<10; i++){
		printf("%d\n", array[i]);
	}

	free(array);
}


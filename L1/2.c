#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(){

	char** pointer = malloc(sizeof(char*)*10);
	int i;

	for (i=0; i<10; i++){
		pointer[i] = malloc(sizeof(char)*15);
		strcpy(pointer[i], "test\0");
	}

	for (i=0; i<10; i++){
		printf("%s\n", pointer[i]);
	}

	free(pointer);
}
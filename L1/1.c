#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(){

	int* pointer = malloc(sizeof(int)*10);
	int i;

	for (i=0; i<10; i++){
		pointer[i] = i;
	}

	for (i=0; i<10; i++){
		printf("%d\n", pointer[i]);
	}

	free(pointer);
}
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct ListNode ListNode; 

struct ListNode
{
	int data;
	struct ListNode* next;
};

void swap(ListNode *a, ListNode *b) {
	
	if((*a).next == b){
		(*a).next = (*b).next;
		(*b).next = a; 
	}
	else if((*b).next == a){
		(*b).next = (*a).next;
		(*a).next = b; 
	}
	
}


ListNode* sort(ListNode *a, int size){
	int i;
	struct ListNode start = *a;
	struct ListNode b = *a;

	*a=start;

	for (i=0; i<size-1; i++){
		while((*a).next != NULL){
			b = *((*a).next);
			if (b.data<(*a).data){
				printf("Swapping %d and %d\n", b.data, (*a).data);
				swap(&b, a);
			}
		}
		*a=start;
	}
	

	return a;
}


int test(){
	struct ListNode a;
	struct ListNode b;
	a.data = 6;
	b.data = 9;
	a.next = &b;

	struct ListNode c=*a.next;

	printf("%d", c.data);
}

int main(){

	struct ListNode a = {.data = 5};
	struct ListNode b = {.data = 7, .next = &a};
	struct ListNode c = {.data = 3, .next = &b};
	struct ListNode d = {.data = 9, .next = &c};
	struct ListNode e = {.data = 1, .next = &d};
	struct ListNode f = {.data = 4, .next = &e};

	struct ListNode iterator = f;

	while(iterator.next != NULL){
		printf("%d\n", iterator.data);
		iterator = *iterator.next;
	}

	printf("\n\n\n\n");

	sort(&f, 6);

	iterator = e;

	while(iterator.next != NULL){
		printf("%d\n", iterator.data);
		iterator = *iterator.next;
	}
}


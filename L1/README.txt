Bryce Eller
CS283 Lab 1

Made in C, tested on Tux. Answers to problems 1-5. To compile the programs, just run the make file with "make"

I think that 4 and 5 don't exactly function as they should.
In 4, I tried to use bubble sort like I did in 3. I had trouble making a swap function
that worked perfectly, even in edge cases. My code is sometimes thrown into an infinite loop.

In 5, I had trouble with pointer management. I think the logic is all correct,
but for some reason, my functions are recreating arrays as opposed to changing the
ones in the given pointer's location. I can't figure out how to make changes that
propogate out of the function.
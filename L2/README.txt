I accomplished all the goals of the assignment.
Each version of the code is clearly labeled, along with its output and an analysis of the results.
Default makefile target runs all the programs. I also made a target named "time" to time each program.

I found the first version to be fastest, but sometimes came to an incorrect total. I had to increase
the amount of iterations because I kept getting accurate results with lower numbers.

The second version was the slowest by far, but it always gave an accurate total count, no matter how
many threads/iterations I used.

The third version was slightly slower than the first but much faster than the second and still gave
accurate results no matter how many threads/iterations were used.
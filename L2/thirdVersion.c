     
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *func( void *ptr );
volatile unsigned int cnt = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

main()
{
     int threadCount=1000;
     pthread_t threads[threadCount];
     int i;
     int  iret1, iret2;

     for(i=0; i<threadCount; i++){
     	pthread_create( &threads[i], NULL, func, (void*) NULL);  //Threads are created and run simultaneously
     }
	for(i=0; i<threadCount; i++){
     	pthread_join( threads[i], NULL); //wait to end the program until all threads are finished
     }

     printf("Cnt: %d", cnt);
     exit(0);
}

void *func( void *ptr )
{
     int i;
     pthread_mutex_lock( &mutex );//The thread locks once, right before the for loop starts. It unlocks again after iterating cnt 10000 times.
     for(i=0; i<10000; i++){
     	cnt++;
     }
     pthread_mutex_unlock( &mutex );//This is faster because it only runs the lock and unlock one time
}


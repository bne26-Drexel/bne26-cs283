
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *func( void *ptr );
volatile unsigned int cnt = 0;


main()
{
     int threadCount=1000;
     pthread_t threads[threadCount];
     int i;
     int  iret1, iret2;

     for(i=0; i<threadCount; i++){
     	pthread_create( &threads[i], NULL, func, (void*) NULL);  //Threads are created and run simultaneously
     }
	for(i=0; i<threadCount; i++){
     	pthread_join( threads[i], NULL); //wait to end the program until all threads are finished
     }

     printf("Cnt: %d", cnt);
     exit(0);
}

void *func( void *ptr )
{
     int i;
     for(i=0; i<10000; i++){
     	cnt++;//One thread could try and use this variable while another thread is already using it. This causes the end value not to be correct
     }
}


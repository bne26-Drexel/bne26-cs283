Bryce Eller
CS283 Lab3

Both of my programs work correctly. I calculate an arbitrary number of airports/carriers in response to questions
1-4, and also print out the average/highest values. You can change the file location with a constant at the top 
of the file named "FILENAME".

I had some trouble changing the code to run in parallel, but I did get it up and running on the Google cloud.
I successfully created the cluster and data bucket and  ran my code using pyspark on the remote server.
My account is cs283-parallelism and my bucket is named bne26.

My pi program uses a monte carlo simulation to compute an estimate for the value of pi. I couldn't figure
out how to get this program to run with different amounts of nodes either, but it works correctly.

I did use numpy for some of the parsing. If you don't have it installed you can install it with the
following commands:
sudo apt-get install python3-pip
pip3 install numpy

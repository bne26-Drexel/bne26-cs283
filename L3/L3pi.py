import csv
import numpy as np
import heapq

def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False



WDelay = 26
SecDelay = 28
lairDelay=29
DPort = 17
carrier = 10

longestSecWeathDelayCount=3;
longestSecWeatDelay=[];
longestSecWeatDelayId=[];
for i in range(longestSecWeathDelayCount):
    longestSecWeatDelay.append(0);
    longestSecWeatDelayId.append(0);

longestCarrierDelayCount=3;
longestCarrierDelay=[];
longestCarrierDelayId=[];
for i in range(longestCarrierDelayCount):
    longestCarrierDelay.append(0);
    longestCarrierDelayId.append(0);

averageAirportDelayCount=3;
airports=[]; #each element is an airport
delaysByAirport=[]; #each element is a list of delays, corresponding to an airport

averageCarrierDelayCount=3;
carriers=[]; # each element is a carrier
delaysByCarrier=[]; #each element is a list of carriers, corresponding to a carrier
  
    
#a) which airports had the longest delays due to security and weather (combined)? 
with open('smallcsv.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
       
    for row in spamreader:
        if (isInt(row[26]) or isInt(row[28])) and (row[26] or row[28]):
            if not isInt(row[26]):
                delay = int(row[28])
            elif not isInt(row[28]):
                delay = int(row[26])
            else:
                delay = int(row[26])+int(row[28])
                
            for i in range(len(longestSecWeatDelay)):
                if delay>longestSecWeatDelay[i]:
                    longestSecWeatDelay[i] = delay
                    longestSecWeatDelayId[i] = row[17] #17=departureAirport
                    break;
    print("Which "+str(longestSecWeathDelayCount) +" airports had the longest security/weather delays?")
    print(longestSecWeatDelay)
    print(longestSecWeatDelayId)

#b) Which carriers had the longest carrier delays? 
with open('smallcsv.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    
    for row in spamreader:
        if isInt(row[25]) and row[25]:
            delay = int(row[25])
                
            for i in range(len(longestCarrierDelay)):
                if delay>longestCarrierDelay[i]:
                    longestCarrierDelay[i] = delay
                    longestCarrierDelayId[i] = row[17] #17=departureAirport
                    break;
    
    print("Which "+str(longestCarrierDelayCount) +" carriers had the longest carrier delays?")
    print(longestCarrierDelay)
    print(longestCarrierDelayId)
    

#c) What was the average total late aircraft delay for each airport?
with open('smallcsv.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

    for row in spamreader:
        if isInt(row[lairDelay]) and row[lairDelay]:
            delay = int(row[lairDelay])
            
            if row[17] not in airports:
                airports.append(row[17])
                delaysByAirport.append([delay])
            else:
                airportId = airports.index(row[17])
                delaysByAirport[airportId].append(delay)
                        
    averageDelays = []
    for i in range(len(airports)):
        averageDelays.append(np.mean(delaysByAirport[i]))
        
    #averageDelays is a list of average delays
    #airports is a list of corresponding airports
    
    topAverageDelays = heapq.nlargest(averageAirportDelayCount, averageDelays)

    averageDelaysAirport = []
    for delay in topAverageDelays:
        delayIndex = averageDelays.index(delay)
        averageDelaysAirport.append(airports[delayIndex])
    
    print("Which "+str(averageAirportDelayCount) +" airports had the longest average total late aircraft delays?")
    print(topAverageDelays)
    print(averageDelaysAirport)


#d) What was the average total late aircraft delay for each carrier?
with open('smallcsv.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

    for row in spamreader:
        if isInt(row[lairDelay]) and row[lairDelay]:
            delay = int(row[lairDelay])
            
            if row[9] not in carriers:
                carriers.append(row[9])
                delaysByCarrier.append([delay])
            else:
                carrierId = carriers.index(row[9])
                delaysByCarrier[carrierId].append(delay)
                        
    averageDelays = []
    for i in range(len(carriers)):
        averageDelays.append(np.mean(delaysByCarrier[i]))
        
    #averageDelays is a list of average delays
    #airports is a list of corresponding airports
    
    topAverageDelays = heapq.nlargest(averageCarrierDelayCount, averageDelays)

    averageDelaysCarrier = []
    for delay in topAverageDelays:
        delayIndex = averageDelays.index(delay)
        averageDelaysCarrier.append(carriers[delayIndex])
    
    print("Which "+str(averageCarrierDelayCount) +" airports had the longest average total late aircraft delays?")
    print(topAverageDelays)
    print(averageDelaysCarrier)
        
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
void createpipe(int *p2c, int *c2p){
	if (pipe(p2c) < 0 || pipe(c2p) < 0){ 
		printf("create pipe error\n");
		exit(1);
		 }
}

void closepipe(const int p2c, const int c2p){
	if (close(p2c) < 0 || close(c2p) < 0){ 
		printf("close pipe error\n");
		exit(1);
	 }
}

int main(int argc, char* argv[]){
	int game = atoi(argv[1]);
	int dimensions = atoi(argv[2]);

	//These are the arrays of pipes through which data will be transferred
	int parentpipes[game][2];
	int childpipes[game][2];
	int c1;
	int pickedcolumn = 5;

	int *board = (int *) calloc(game*dimensions*dimensions,sizeof(int));
	int **parentboard = (int**) malloc(game*sizeof(*board));
	pid_t pidlist[game];
	//creating each set of pipes
	for(c1 = 0; c1 < game; c1++){
		createpipe(parentpipes[c1],childpipes[c1]);
	}

	//This loop creates each child process with fork
	//Child process ids are stored in pidlist
	for(c1 = 0; c1 < game; c1++){
		pidlist[c1]=fork();
		if (pidlist[c1] < 0){ 
			printf("fork error\n");
			exit(1);
		}
		//Child Process
		else if(pidlist[c1] == 0 ){
			closepipe(parentpipes[c1][1],childpipes[c1][0]);
			int *buf = malloc(sizeof(int));
			int *move = malloc(sizeof(int));
			int done = 0;
			int *x = malloc(sizeof(int));
			int *y = malloc(sizeof(int));
			while(!done){
				
				printf("child %d is waiting to read\n", c1+1);
				move = (int*)(uintptr_t)read(parentpipes[c1][0], &buf, sizeof(int));
				

				printf("child %d received data: %d", c1+1, *move);
				write(childpipes[c1][1], pickedcolumn, sizeof(int));
				
			}
			printf("child %d is done\n", c1);
			closepipe(parentpipes[c1][0],childpipes[c1][1]);
			exit(0);
		}
		else{
			//parent process
			printf("Child %d created \n", c1+1);
		}
	}

	//parent process
	int alldone = 0;
	while(!alldone){
		//printing move
		int *buf = malloc(sizeof(int));
		int *move = 0;
		int pickedcolumn = 0;
		int gamenum;

		//Parent should send a move to the child, then wait for a response. After it receives a response, it moves on to the next child

		for(gamenum = 0; gamenum < game; gamenum++){
			pickedcolumn = 3;

			write(parentpipes[gamenum][1], &pickedcolumn, sizeof(int));
			printf("Parent is waiting to read from child %d", gamenum+1); //Maybe this creates a deadlock?

			move = (int*)(uintptr_t)read(childpipes[gamenum][0],&buf, sizeof(int));
			printf("parent received data from child %d: %d", gamenum+1, *move);
			
		}
	}

	return 0;
}


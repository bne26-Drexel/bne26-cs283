Bryce Eller
CS360 - A3 - Four-in-a-row

My program works partially. The makefile works as expected (finally got your arguments to work!).
If you use GAMES=4 DIMENSION=8 make run, the program runs with no reported errors. 
Default build uses 4 and 8 as arguments. I don't have any test cases and there are no intermediate files to clean.

This program creates the necessary child processes as expected. It stores their ids to an array, and opens up
a set of two pipes for each process. It fails to actually get the data to send concurrently. It seems to me
that the child processes are waiting to read data for an indefinite amount of time and the parent process just
isn't sending the data for some reason.

I think the program deadlocks because the parent sends the data before the child has a chance to read it.
It also could be that the parent is waiting to recieve data before the child sends it.
Either way I can't figure out how to fix this issue. 